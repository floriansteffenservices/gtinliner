# gtinliner #

gtinliner inlines a Go template (or a set of templates) in a Go source file. It is intended to be used with `go generate`. The template syntax is described on the [Go documentation](http://golang.org/pkg/text/template).

## Installation ##

A standard Go development environment must be installed and configured. Then, gtinliner can be installed :

    go install bitbucket.org/floriansteffenservices/gtinliner.git

## Usage ##

    gtinliner [arguments] [files ...]

    -h	Print help and exit.
    -output string
        Write source code to <output>. (default "templates.go")
    -package string
        Use <package> for the package clause of the generated source. (default "main")
    -var string
        Use <var> as variable name holding the templates. (default "templates")

## Generated Source Code Usage ##

Each input file will be available in a template named after the file. All templates are then compiled in an outer template named after <var> and referenced by a variable named <var>. With a set of 3 templates `A.tmpl`, `B.tmpl` and `C.tmpl` and by executing `gtinliner A.tmpl B.tmpl C.tmpl` will generate a templates.go file. Then, for example, template A can be executed like this:

`err := templates.ExecuteTemplate(wr, "A.tmpl", data)`

## Example ##

The `example` folder shows how to use `gtinliner` with `go generate`. It can be built with `go generate` and `go build`.

## License ##

The project is licensed under the term of the Modified BSD License. See LICENSE file for more details.
