package main

import (
	"bytes"
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"text/template"
)

var inlinerTmpl string = `package {{.package}}

import "text/template"

var {{.var}} *template.Template

func init() {
	{{.var}} = template.Must(template.New("{{.var}}").Parse(` + "`{{.tmpl}}`" + `))
}`

func printHelpAndExit(errcode int) {
	fmt.Fprintln(os.Stderr, `gtinliner - Golang templates inliner 1.0

usage : gtinliner [arguments] [file ...]
`)
	flag.PrintDefaults()
	os.Exit(errcode)
}

func printErrAndExit(format string, a ...interface{}) {
	fmt.Fprintf(os.Stderr, format, a...)
	fmt.Fprintln(os.Stderr, "")
	os.Exit(2)
}

func main() {
	packageName := flag.String("package", "main", "Use <package> for the package clause of the generated source.")
	varName := flag.String("var", "templates", "Use <var> as variable name holding the templates.")
	outputName := flag.String("output", "templates.go", "Write source code to <output>.")
	help := flag.Bool("h", false, "Print help and exit.")

	flag.Parse()

	if *help {
		printHelpAndExit(0)
	}
	if flag.NArg() == 0 {
		printHelpAndExit(2)
	}

	var tmpls bytes.Buffer
	for _, f := range flag.Args() {
		name := filepath.Base(f)
		if name[0] == '.' {
			continue
		}
		tmpls.WriteString(fmt.Sprintf("{{define \"%s\"}}", name))
		bytes, err := ioutil.ReadFile(f)
		if err != nil {
			printErrAndExit("Can't read from %s. %s", f, err)
		}
		tmpls.Write(bytes)
		tmpls.WriteString("{{end}}\n")
	}

	_, err := template.New("templates").Parse(tmpls.String())
	if err != nil {
		printErrAndExit("Can't parse templates. %s", err)
	}

	output, err := os.Create(*outputName)
	if err != nil {
		printErrAndExit("Can't create output file.  %s", err)
	}
	defer output.Close()

	inliner, err := template.New("inliner").Parse(inlinerTmpl)
	if err != nil {
		printErrAndExit("Can't parse inliner template. %s", err)
	}

	err = inliner.Execute(output, map[string]string{
		"tmpl":    tmpls.String(),
		"var":     *varName,
		"package": *packageName})
	if err != nil {
		printErrAndExit("Can't write output. %s", err)
	}
}
