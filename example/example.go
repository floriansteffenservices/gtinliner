package main

//go:generate gtinliner -package $GOPACKAGE A.tmpl B.tmpl C.tmpl

import "os"

func main() {
	templates.ExecuteTemplate(os.Stdout, "A.tmpl", nil)
	templates.ExecuteTemplate(os.Stdout, "B.tmpl", nil)
	templates.ExecuteTemplate(os.Stdout, "C.tmpl", nil)
	// should print A B C
}
